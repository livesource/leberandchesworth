const gulp = require("gulp");
const order = require("gulp-order");
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");
const autoprefixer = require("gulp-autoprefixer");
const babel = require("gulp-babel");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const imagemin = require("gulp-imagemin"),
cache = require("gulp-cache");
const minifycss = require("gulp-minify-css");
const sass = require("gulp-sass");
const browsersync = require("browser-sync").create();

// BrowserSync
function bsInit(done) {
  browsersync.init({
    port: 8080,
    proxy: "https://leberandchesworth.test"
  });
  done();
}

// BrowserSync Reload
function bsReload(done) {
  browsersync.reload();
  done();
}

// Optimize Images
function images() {
  "use strict";
  return gulp
    .src("src/images/**/*")
    .pipe(
      cache(
        imagemin([
          imagemin.gifsicle({
            interlaced: true,
          }),
          imagemin.mozjpeg({
            progressive: true,
          }),
          imagemin.optipng({
            optimizationLevel: 5,
          }),
          imagemin.svgo({
            plugins: [
              {
                removeViewBox: true,
              },
            ],
          }),
        ])
      )
    )
    .pipe(gulp.dest("dist/images/"));
}

// Compile CSS
function styles() {
  "use strict";
  return gulp
    .src(["src/styles/**/*.scss"])
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gulp.dest("dist/styles/"))
    .pipe(
      rename({
        suffix: ".min",
      })
    )
    .pipe(minifycss())
    .pipe(gulp.dest("dist/styles/"))
    .pipe(browsersync.stream());
}

// Concatenate, minify, and lint scripts
function scripts() {
  "use strict";
  return gulp
    .src("src/scripts/**/*.js")
    .pipe(order([
      'plugins/*.js',
      '*.js'
    ]))
    .pipe(plumber())
    .pipe(gulp.dest("dist/scripts/"))
    .pipe(concat("main.js"))
    .pipe(gulp.dest("dist/scripts/"))
    .pipe(
      rename({
        suffix: ".min",
      })
    )
    .pipe(uglify())
    .pipe(gulp.dest("dist/scripts/"))
    .pipe(browsersync.stream());
}

// Watch Files
function watchFiles() {
  "use strict";
  gulp.watch("src/styles/**/*.scss", styles);
  gulp.watch("src/scripts/**/*.js", scripts);
  gulp.watch("*.html", bsReload);
  gulp.watch("src/images/**/*", images);
}

// Group complex tasks
const build = gulp.parallel(styles, images, scripts);
const watch = gulp.parallel(styles, images, scripts, watchFiles, bsInit);

// Export tasks
exports.build = build;
exports.images = images;
exports.scripts = scripts;
exports.styles = styles;
exports.watch = watch;
exports.default = watch;
