<!DOCTYPE html>

<!--[if lt IE 7 ]><html lang="en" class="no-js no-touch lte7 lte8 lte9 lte10"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js no-touch lte8 lte9 lte10"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js no-touch lte9 lte10"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js no-touch lte10"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js no-touch"><!--<![endif]-->

	<head>
		<% base_tag %>
		<title><% if $MetaTitle %>$MetaTitle<% else %>$Title<% end_if %> | $SiteConfig.Title</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<meta name="format-detection" content="telephone=no" />
		$MetaTags(false)

		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png?v=2">
		<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png?v=2">
		<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png?v=2">
		<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png?v=2">
		<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png?v=2">
		<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png?v=2">
		<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png?v=2">
		<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png?v=2">
		<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png?v=2">
		<link rel="icon" type="image/png" href="favicon/favicon-32x32.png?v=2" sizes="32x32">
		<link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png?v=2" sizes="192x192">
		<link rel="icon" type="image/png" href="favicon/favicon-96x96.png?v=2" sizes="96x96">
		<link rel="icon" type="image/png" href="favicon/favicon-16x16.png?v=2" sizes="16x16">
		<link rel="manifest" href="favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png?v=2">
		<meta name="theme-color" content="#ffffff">
		<link href='https://fonts.googleapis.com/css?family=Dosis:600' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="$resourceURL(themes/default/dist/styles/screen.min.css)">
	</head>
	<body class='$CSSClass gallery_closed<% if ClassName == NewsHolder %> loading<% end_if %>'>