$(function () {
  var gallery = $(".gallery_items");

  if (!gallery.length) return;

  var galleryItems = gallery.find(".gallery_item");

  var wrapper = $(".wrapper");
  var galleryHeight;

  var previousItem;

  var togglingGallery = false;

  var inFullScreen = false;

  $(".gallery_overlay").on("click swipe mousedown", function (e) {
    e.preventDefault();
    if (!galleryOpen()) {
      e.stopPropagation();
      toggleGallery();
    }
  });

  setupGallery();
  gallery.owlCarousel({
    loop: false,
    margin: 50,
    nav: true,
    items: 1,
    touchDrag: true,
    mouseDrag: true,
    onPlayVideo: function () {
      if (!galleryOpen()) {
        toggleGallery();
      }
    },
    onDragged: function () {
      if (!galleryOpen()) {
        toggleGallery();
        return;
      }

      var currentItem = gallery.find(".owl-item.active");
      // if we are on the first item dragging back, close gallery
      if (this.state.direction == "right" && currentItem.index() === 0) {
        toggleGallery();
        previousItem = null;
      }
    },
    onChanged: function (e) {
      var currentItem = gallery.find(".owl-item.active");
      if (currentItem) {
        previousItem = currentItem;
      }

      if (currentItem.index() >= 0) {
        $("body").addClass("gallery_prog1");
      } else {
        $("body").removeClass("gallery_prog1");
      }

      if (currentItem.next().find(".gallery_item_end").length) {
        $("body").addClass("last_slide");
      } else {
        $("body").removeClass("last_slide");
      }

      setTimeout(function () {
        checkToHideNextBtn();
      }, 100);
    },
  });

  app.nextbtn.click(function (e) {
    var self = $(this);
    if (galleryOpen()) {
      // if we're on the last item and this button exists, then we should nav to next
      if (
        gallery.find(".owl-item:last").hasClass("active") &&
        self.attr("href").indexOf("#") < 0
      ) {
        $("body").addClass("loading");
        return true;
      }
      gallery.trigger("next.owl.carousel");
    } else {
      toggleGallery();
      checkToHideNextBtn();
    }
    return false;
  });

  app.prevbtn.click(function () {
    if (gallery.find(".owl-item:first").hasClass("active")) {
      // retract the gallery if we are clicking prev when on the first item
      toggleGallery();
      if (app.menupanel.isOpen()) {
        app.menupanel.toggle();
      }
    } else {
      gallery.trigger("prev.owl.carousel");
    }
    showNextBtn();

    return false;
  });

  $(".reset_gallery").click(function () {
    resetGallery();
    return false;
  });

  $(window).on("resized", function () {
    setupGallery();
  });

  $(document).on(
    "webkitfullscreenchange mozfullscreenchange fullscreenchange MSFullscreenChange",
    function () {
      inFullScreen = inFullScreen ? false : true;
    }
  );

  function setupGallery() {
    if (inFullScreen) return;

    var winWidth = $(window).width();

    var less = winWidth < app.breakpoints.medium ? 40 : 100;
    gallery.css({
      width: $(window).width() - less,
    });
    galleryHeight = $(window).height() - less;
    galleryItems.css({
      height: galleryHeight,
    });
    isSetup = true;
  }

  function toggleGallery() {
    if (togglingGallery) return false;

    toggleingGallery = true;

    if (!galleryOpen()) {
      // lazy load gallery images
      $(".gallery_delay_image_loading img").trigger(
        "gallery_delay_image_loading"
      );
    } else {
      $("body").removeClass("gallery_prog1");
    }

    var winWidth = $(window).width();
    var less = winWidth < app.breakpoints.medium ? 20 : 50;

    var left = gallery.offset().left - less;
    wrapper.animate(
      {
        left: "-" + left,
      },
      0
    );
    $("body").toggleClass("gallery_open").toggleClass("gallery_closed");

    toggleingGallery = false;
  }

  function resetGallery() {
    gallery.trigger("to.owl.carousel", 0);
    toggleGallery();
  }

  function checkToHideNextBtn() {
    // if we have a next page, don't hide the next button
    if (app.nextbtn.hasClass("has_next_page")) return;

    if (gallery.find(".owl-item:last").hasClass("active")) {
      if (galleryOpen()) {
        hideNextBtn();
      }
    } else {
      showNextBtn();
    }
  }

  function galleryOpen() {
    return $("body").hasClass("gallery_open");
  }

  function showNextBtn() {
    app.nextbtn.animate({
      right: 0,
    });
  }

  function hideNextBtn() {
    app.nextbtn.animate({
      right: "-40px",
    });
  }

  $(".delay_image_loading img").lazyload({
    event: "delay_image_loading",
  });
});
