$(function () {
  $container = $(".news_articles").first();
  $container.infinitescroll(
    {
      debug: false,
      navSelector: ".pagination",
      nextSelector: ".pagination a.next",
      itemSelector: ".article",
      extraScrollPx: 250,
      pathParse: function (path, number) {
        return path;
      },
      path: function (number) {
        var el = $(".news_articles");
        var link = el.data("link");
        var num = Number(el.data("itemsperpage"));
        return link + "?start=" + (number - 1) * num;
      },
      errorCallback: function () {},
      loading: {
        msgText: "",
        finishedMsg: "",
        img: "themes/default/dist/images/ajax-loader.gif",
        selector: "#pagination_loading",
      },
    },
    function (newElements) {
      $container.isotope("appended", $(newElements));
      setTimeout(function () {
        $container.isotope("layout");
      }, 1000);
    }
  );
  $container.isotope({
    itemSelector: ".article",
    layout: "fitColumns",
  });

  setTimeout(function () {
    $container.isotope("layout");
  }, 1000);

  $(window).on("resized", function () {
    $container.isotope("layout");
  });

  $("a.back_to_top").click(function () {
    $("html, body").animate(
      {
        scrollTop: 0,
      },
      500
    );
    return false;
  });
});

