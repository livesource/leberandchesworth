/*! Picturefill - Responsive Images that work today. (and mimic the proposed Picture element with span elements). Author: Scott Jehl, Filament Group, 2012 | License: MIT/GPLv2 */

(function( w ){

	// Enable strict mode
	"use strict";

	w.picturefill = function() {
		var ps = w.document.getElementsByTagName( "span" );

		// Loop the pictures
		for( var i = 0, il = ps.length; i < il; i++ ){
			if( ps[ i ].getAttribute( "data-picture" ) !== null ){

				var sources = ps[ i ].getElementsByTagName( "span" ),
					matches = [];

				// See if which sources match
				for( var j = 0, jl = sources.length; j < jl; j++ ){
					var media = sources[ j ].getAttribute( "data-media" );
					// if there's no media specified, OR w.matchMedia is supported 
					if( !media || ( w.matchMedia && w.matchMedia( media ).matches ) ){
						matches.push( sources[ j ] );
					}
				}

			// Find any existing img element in the picture element
			var picImg = ps[ i ].getElementsByTagName( "img" )[ 0 ];

			if( matches.length ){
				var matchedEl = matches.pop();
				if( !picImg || picImg.parentNode.nodeName === "NOSCRIPT" ){
					picImg = w.document.createElement( "img" );
					picImg.alt = ps[ i ].getAttribute( "data-alt" );
				}

				// should we lazyload?
				var parentLazy = jQuery(matchedEl).parents('.lazy');
				if(jQuery.fn.lazyload && parentLazy.length){
					// check if src has changed
					if(picImg.getAttribute('data-original') != matchedEl.getAttribute( "data-src" )){
						// set data-original attr for lazyload.js
						picImg.setAttribute('data-original', matchedEl.getAttribute( "data-src" ));
						// set width and height for lazyload.js placeholder
						picImg.setAttribute('width', matchedEl.getAttribute( "data-width" ));
						picImg.setAttribute('height', matchedEl.getAttribute( "data-height" ));
						// append the new image
						matchedEl.appendChild( picImg );
						// tell lazyload.js about our new/modified image
						if(parentLazy.hasClass('gallery_delay_image_loading')){
							$(picImg).lazyload({ event : "gallery_delay_image_loading" }).load(function() {
							    var self = $(this);
							    if(self.attr('src') == self.data('original')){
							    	self.addClass('lazy_loaded');
							    }
							});	
						}else{
							$(picImg).lazyload({ effect : "fadeIn" }).load(function() {
							    var self = $(this);
							    if(self.attr('src') == self.data('original')){
							    	self.addClass('lazy_loaded');
							    }
							});		
						}
						
					}
				}else{
					// default picturefill functionality
					picImg.src =  matchedEl.getAttribute( "data-src" );
					matchedEl.appendChild( picImg );
				}
			}
			else if( picImg ){
				picImg.parentNode.removeChild( picImg );
			}
		}
		}
	};

	// Run on resize and domready (w.load as a fallback)
	if( w.addEventListener ){
		w.addEventListener( "resize", w.picturefill, false );
		w.addEventListener( "DOMContentLoaded", function(){
			w.picturefill();
			// Run once only
			w.removeEventListener( "load", w.picturefill, false );
		}, false );
		w.addEventListener( "load", w.picturefill, false );
	}
	else if( w.attachEvent ){
		w.attachEvent( "onload", w.picturefill );
	}

}( this ));
