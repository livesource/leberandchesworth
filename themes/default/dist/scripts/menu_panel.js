;(function ($, window) {	
	$(function(){
		app.menupanel = {};
		app.menupanel.menu = $('.menu_panel nav');
		app.menupanel.menu.find('li li a').hover(function(){
			app.menupanel.menu.toggleClass('highlight_current');
		});

		$(".hamburger").click(function() {
		    app.menupanel.toggle();
		});

		app.menupanel.toggle = function(){
			app.body.toggleClass("menu_panel_active");
		}

		app.menupanel.isOpen = function(){
			return app.body.hasClass("menu_panel_active");
		}
		
	});
})(jQuery, window);