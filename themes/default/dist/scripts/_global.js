// global html
var app = {};
window.onunload = function(){};

$(function() {
	app.html = $('html');
	app.body = $('body').removeClass('loading');
	app.nextbtn = $('.navbtn_next');
	app.prevbtn = $('.navbtn_prev');
	app.breakpoints = {
	  	xxlarge: 1360,
	  	xlarge: 1136,
		large: 945,
		medium: 768,
		small: 545,
		xsmall: 0
	};

	//load modernizr and any polyfills
	var modernizrLoaded = setInterval(function() {
		if (typeof Modernizr !== 'undefined') {
			clearInterval(modernizrLoaded);

			// adapt html tag classes
			app.html.addClass('js').removeClass('no-js');
			if (Modernizr.touch) app.html.addClass('touch').removeClass('no-touch');
	  	}
	}, 50);

	// internal links show loading screen on click
	var $externalLinks = $("a[href^='http://'], a[href^='https://'], a[href^='mailto:'], a[href$='PDF'], a[href$='.pdf'], a[target='_blank']");
	$externalLinks.addClass('noload');
	$('a').click(function(){
		var self = $(this);
		if(!self.hasClass('noload')){
			app.body.addClass('loading');
		}
		return true;
	});

	// add resized event to window - example usage
	// $(window).on('resized', function(){
	//     	dosomething();
	// });
    var windowResizeTime;
    window.onresize = function(){
        clearTimeout(windowResizeTime);
        windowResizeTime = setTimeout(function(){
        	$(window).trigger('resized');
        }, 100);
    };

});
