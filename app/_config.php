<?php

use SilverStripe\Admin\CMSMenu;
use SilverStripe\CampaignAdmin\CampaignAdmin;
use SilverStripe\Core\Config\Config;
use SilverStripe\Security\PasswordValidator;
use SilverStripe\Security\Member;
use SilverStripe\View\Requirements;
use Symbiote\QueuedJobs\Controllers\QueuedJobsAdmin;

// remove PasswordValidator for SilverStripe 5.0
$validator = PasswordValidator::create();
// Settings are registered via Injector configuration - see passwords.yml in framework
Member::set_password_validator($validator);

CMSMenu::remove_menu_class(CampaignAdmin::class);
CMSMenu::remove_menu_class(Symbiote\QueuedJobs\Controllers\QueuedJobsAdmin::class);

Requirements::set_force_js_to_bottom(true);
