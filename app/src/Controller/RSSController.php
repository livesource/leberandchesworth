<?php

namespace App\Web\Controller;

use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Control\RSS\RSSFeed;
use SilverStripe\Control\Director;
use SilverStripe\CMS\Controllers\ContentController;

class RSSController extends ContentController{

	const URLSegment = 'rss';

	private static $page_types = array(
		BlogPost::class,
	);

	/**
	 * Get the rss feed entries
	 */
	function index() {
		$title = $this->SiteConfig()->Title . ' RSS Feed';
		$entries = BlogPost::get()
			->sort('PublishDate DESC')
			->limit('20');

		if($entries) {
			$rss = RSSFeed::create($entries, $this->Link(), $title, "", "Title", 'Content', "Author");
			return $rss->outputToBrowser();
		}
	}

	/*
	 * The URL accessable link to this RSS feed
	 */
	function Link($action = null){
		return Director::absoluteBaseURL() . '/rss/' . $action;
	}
}