<?php

namespace App\Web\Extension;

use SilverStripe\ORM\DataExtension;

class CustomFile extends DataExtension
{
	public function Lazy(){
		return $this->owner->renderWith('LazyImage');
	}
}