<?php

namespace App\Web\Extension;

use Psr\SimpleCache\CacheInterface;
use SilverStripe\Control\Director;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Core\Extension;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Dev\Debug;

/**
 * CustomGridFieldOrderableRows
 *
 * deletes cache for page when gallery items are re-ordered
 *
 */
class CustomGridFieldOrderableRows extends Extension
{
    public function onAfterReorderItems($list, $values, $sortedIDs)
    {
        // $page = $list->first()->Page();
        // if ($page && $page->isPublished()) {
        //     // $page->purgeStaticCache();
        //     $page->collectChanges(['action' => 'publish']);
        //     $page->flushChanges();
        // }
    }
}