<?php

namespace App\Web\Extension;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;

class CustomSiteConfig extends DataExtension
{
	private static $db = [];

	function updateCMSFields(FieldList $fields)
    {
		$fields->removeByName('Tagline');
	}
}
