<?php

namespace App\Web\Page;

use SilverStripe\Assets\Image;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Core\Convert;
use SilverStripe\Control\Director;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\ErrorPage\ErrorPage;
use SilverStripe\ORM\DataObject;
use SilverStripe\View\SSViewer;
use SilverStripe\View\Requirements;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\FormAction;
use SilverStripe\CMS\Search\SearchForm;
use SilverStripe\Control\Controller;
use SilverStripe\Versioned\Versioned;
use SilverStripe\Control\Session;
use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Control\Middleware\HTTPCacheControlMiddleware;
use SteadLane\Cloudflare\Purge;

class BasePage extends SiteTree
{
    private static $table_name = 'BasePage';

    private static $db = [
        "MetaTitle" => "Varchar(255)",
    ];

    private static $has_one = [
        'Image' => Image::class,
    ];

    private static $image_default = 'default_image.jpg';

    private static $has_image = false;

    /**
     * Limit the number of pages of this type that can be created
     * @var int
     **/
    private static $limit_pages;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $metaComposite = $fields->fieldByName('Root.Main.Metadata');
        $metaComposite->insertBefore(
            TextField::create('MetaTitle')
                ->setAttribute('placeholder', $this->Title),
            'MetaDescription'
        );

        if ($this->config()->get('has_image')) {
            $fields->addFieldToTab(
                "Root.Images",
                $imageField = UploadField::create('Image', 'Image')
                    ->setAllowedFileCategories('image')
                    ->setAllowedMaxFileNumber(1)
                    ->setFolderName($this->config()->get('image_folder'))
            );
            $imageField->getValidator()->setAllowedMaxFileSize(2097152);
        }

        return $fields;
    }

    /**
     * AFMetaTags function. Overridden to allow custom meta description
     *
     * @return string $tags
     */
    public function MetaTags($includeTitle = true)
    {
        $tags = parent::MetaTags($includeTitle);

        if (!$this->MetaDescription) {
            $tags .= "<meta name=\"description\" content=\"" . Convert::raw2att($this->getMetaDesc()) . "\" />\n";
        }

        return $tags;
    }


    /**
     * The title for this page, used in meta tag
     * @return string
     **/
    function PageTitle()
    {
        $title = $this->MetaTitle ? $this->MetaTitle : $this->Title;
        return $title . ' | ' . $this->getSiteConfig()->Title;
    }


    /**
     * Fallback to summary or content for meta description tag
     * @return string
     **/
    function getMetaDesc()
    {
        return $this->MetaDescription ?
            $this->MetaDescription :
            Convert::raw2att($this->SummaryOrContent()->Summary(30));
    }


    /**
     * Image or default for opengraph
     * @return String
     **/
    public function getOGImage()
    {
        $image = $this->ImageOrDefault(true);
        if ($image && $image->exists()) return $image->getAbsoluteURL();
    }


    /**
     * desription for opengraph
     * @return String
     **/
    public function getOGDescription()
    {
        return $this->getMetaDesc();
    }


    /**
     * Summary or content
     * @return DBField
     **/
    public function SummaryOrContent()
    {
        return $this->hasField('Summary') ? $this->dbObject('Summary') : $this->dbObject('Content');
    }


    /**
     * Gets the image for this page or falls back to default
     * @param boolean $fallbackInLive
     * @return Image
     **/
    function ImageOrDefault($fallbackInLive = false)
    {
        if (!$fallbackInLive && Director::isLive()) return $this->Image();
        return $this->ImageID ? $this->Image() : $this->DefaultImage();
    }


    /**
     * Gets the default image
     * @return Image
     **/
    function DefaultImage()
    {
        return Image::get()->filter('Name', $this->config()->image_default)->first();
    }


    /**
     * Get sibling pages of this page
     * @return DataList
     **/
    public function getSiblings()
    {
        return SiteTree::get()
            ->filter('ParentID', $this->ParentID)
            ->exclude(array(
                'ID' => $this->ID,
                'ClassName' => ErrorPage::class,
            ));
    }


    /**
     * Get the next page
     * @return SiteTree
     **/
    public function Next()
    {
        if ($this->Sort !== null) {
            return $this->getSiblings()->where("Sort > $this->Sort")->first();
        }
    }


    /**
     * Get the previous page
     * @return SiteTree
     **/
    public function Prev()
    {
        return $this->getSiblings()->where("Sort < $this->Sort")->last();
    }

    function canCreate($member = null, $context = [])
    {
        if ($limit = $this->config()->get('limit_pages')) {
            return DataObject::get($this->owner->ClassName)->count() < $limit;
        }

        return true;
    }

    /**
     * A css class to apply to the <body>
     * @return string
     */
    public function CSSClass()
    {
        return 'type_' . strtolower($this->ClassName);
    }

    public function publishRecursive()
    {
        Purge::singleton()->quick('page', $this->ID);
        return parent::publishRecursive();
    }
}

class BasePageController extends ContentController
{

    private static $allowed_actions = [
        'dismiss_browser_rejection',
        'SearchForm'
    ];


    /**
     * Stores file extension prefix for js and css files
     * will either be empty or ".min"
     * @var string
     **/
    protected $min = '';


    public function init()
    {
        HTTPCacheControlMiddleware::singleton()
            ->publicCache(true)
            ->setMaxAge(60 * 60 * 24 * 7 * 52); // 1 year

        parent::init();
        Requirements::set_combined_files_folder('themes/default/dist');
        $this->min = !Director::isDev() ? '.min' : '';
    }

    /**
     * Used for conditional partial caching
     * @return Boolean
     **/
    public function isLive()
    {
        return Director::isLive();
    }
}
