<?php

namespace {

    use App\Web\Model\GalleryItem;
    use App\Web\Page\BasePage;
    use Axllent\FormFields\Forms\NoticeField;
    use Colymba\BulkUpload\BulkUploader;
    use SilverStripe\Blog\Model\Blog;
    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Control\Director;
    use SilverStripe\ErrorPage\ErrorPage;
    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\GridField\GridField;
    use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
    use SilverStripe\Forms\GridField\GridFieldDeleteAction;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\ToggleCompositeField;
    use SilverStripe\Forms\TreeDropdownField;
    use SilverStripe\Versioned\Versioned;
    use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

    class Page extends BasePage
    {
        private static $table_name = 'Page';

        private static $hide_ancestor = BasePage::class;

        private static $db = [
            'Intro' => 'HTMLText',
            'Content2' => 'HTMLText',
            'LayoutType' => 'Varchar',
        ];

        private static $has_one = [
            'PageToGoNext' => SiteTree::class
        ];

        private static $has_many = [
            'GalleryItems' => GalleryItem::class
        ];

        private static $layout_types = [
            'SingleColumn' => 'Single Column',
            'TwoColumn' => 'Two Column',
            'Gallery' => 'Gallery',
        ];

        private static $defaults = [
            'LayoutType' => 'SingleColumn'
        ];

        private static $has_image = false;

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();

            if($this->ClassName == Page::class){
                $fields->addFieldToTab(
                    'Root.Main',
                    DropdownField::create(
                        'LayoutType',
                        'Layout Type',
                        $this->config()->layout_types
                    ),
                    'Content'
                );
            }

            $fields->addFieldToTab(
                'Root.Main',
                ToggleCompositeField::create(
                    'IntroToggle',
                    'Intro Text/Subtitle (optional)',
                    [HTMLEditorField::create('Intro', 'Intro Text/Subtitle (optional)')]
                ),
                'Content'
            );

            if($this->ClassName == Page::class){
                $fields->addFieldToTab(
                    'Root.Main',
                    HTMLEditorField::create('Content2', 'Content (Column 2)')
                        ->displayIf('LayoutType')->isEqualTo('TwoColumn')->end(),
                    'Metadata'
                );

                if($this->Type == 'TwoColumn'){
                    $fields->dataFieldByName('Content')->setTitle('Content (Column 1)');
                }

                $bulkUploader = new BulkUploader();
                $bulkUploader->setUfSetup('setFolderName', "gallery/$this->ID");
                $bulkUploader->setUfSetup('setAttachEnabled', false);

                $gridConfig = GridFieldConfig_RelationEditor::create()
                    ->addComponent(new GridFieldOrderableRows())
                    ->addComponent($bulkUploader)
                    ->removeComponentsByType('GridFieldAddExistingAutocompleter')
                    ->removeComponentsByType(GridFieldDeleteAction::class)
                    ->addComponent(new GridFieldDeleteAction(false));


                $fields->addFieldsToTab(
                    'Root.Gallery',
                    [
                        NoticeField::create('Notice','When using the upload field below, you may get an error message. Please ignore it, you should find when you save that they are all uploaded successfully.', 'notice', false),
                        GridField::create(
                            'GalleryItems',
                            'Gallery Items',
                            $this->GalleryItems(),
                            $gridConfig
                        )
                    ]
                );
            }

            $fields->addFieldToTab(
                'Root.Main',
                TreeDropdownField::create(
                    'PageToGoNextID',
                    'Page to go to next',
                    SiteTree::class,
                    'ID',
                    'MenuTitle'
                )->setRightTitle('Leave empty to go to the natural next page'),
                'IntroToggle'
            );

            return $fields;
        }


        public function NextPage()
        {
            if($this->ClassName == Blog::class) return false;

            if($this->PageToGoNext()->exists()){
                return $this->PageToGoNext();
            }

            if($this->AllChildren()->exists()){
                return $this->AllChildren()->first();
            }

            return $this->Next();
        }


        public function PrevPage()
        {
            if($this->Parent()->ParentID){
                $prev = $this->Prev();
                return $prev ? $prev : $this->Parent();
            }
        }

        /**
         * A css class to apply to the <body>
         * @return string
         */
        public function CSSClass()
        {
            $classes = parent::CSSClass();
            $classes .= ' ' . strtolower($this->LayoutType) . '_layout';
            return $classes;
        }
    }
}
