<?php

namespace App\Web\Page;

use Page;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Control\Controller;
use PageController;


class FirstChildRedirect extends Page
{
    private static $table_name = 'FirstChildRedirectPage';

	private static $description = "Automatically redirects to the first child of this page";

	private static $icon_class = 'font-icon-p-redirect';

	public function getCMSFields()
    {
    	$fields = parent::getCMSFields();

 		$fields->addFieldToTab("Root.Main", new LiteralField("Desc","<h2>First Child Redirect Page</h2><p>This page automatically redirects to it's first child page.</p>"));
		$fields->removeFieldFromTab("Root.Main","Content");
		$fields->removeFieldFromTab("Root.Main","Metadata");

		return $fields;
   }

    public function Link($action = null)
    {
        $child = $this->Children()->first();
        return $child ? $child->Link() : parent::Link();
    }

}

class FirstChildRedirectController extends PageController
{

}