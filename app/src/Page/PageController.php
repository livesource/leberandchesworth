<?php

namespace {

    use App\Web\Page\BasePageController;
    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\View\Requirements;

    class PageController extends BasePageController
    {

        public function init()
        {
            parent::init();
            Requirements::block('heyday/silverstripe-responsive-images:javascript/picturefill/picturefill.min.js');
        }
    }
}
