<?php

namespace App\Web\Page;

use Page;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TreeDropdownField;
use PageController;

class HomePage extends Page
{
    private static $table_name = 'HomePage';

	private static $has_one = [
		'FeaturedProject' => SiteTree::class
    ];

	public function getCMSFields()
    {
		$fields = parent::getCMSFields();

		$fields->addFieldToTab(
			"Root.Main",
			TreeDropdownField::create('FeaturedProjectID', 'Project to display', SiteTree::class, 'ID', 'MenuTitle'),
			'Metadata'
		);

		$fields->removeByName('Content');
		$fields->removeByName('Intro');

		return $fields;
	}

	public function onBeforeWrite()
    {
		parent::onBeforeWrite();
		// force url segment to be home
		$this->URLSegment = 'home';
	}

    /**
     * A css class to apply to the <body>
     * @return string
     */
    public function CSSClass()
    {
    	if ($project = $this->FeaturedProject()) {
    		$classes = parent::CSSClass();
        	$classes .= ' ' . strtolower($project->LayoutType) . '_layout';
        	return $classes;
    	} else {
    		return parent::CSSClass();
    	}
    }
}

class HomePageController extends PageController
{

}