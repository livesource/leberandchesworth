<?php

namespace App\Web\Model;

use EmbeddedObjectField;
use gorriecoe\Embed\Models\Video;
use Sheadawson\Linkable\Forms\EmbeddedObjectField as FormsEmbeddedObjectField;
use Sheadawson\Linkable\Models\EmbeddedObject;
use SilverShop\HasOneField\HasOneButtonField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Assets\Image;
use SilverStripe\Control\Controller;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\DataObject;
use UncleCheese\DisplayLogic\Forms\Wrapper;

class GalleryItem extends DataObject
{
    private static $table_name = 'GalleryItem';

	private static $db = [
		'Type' => "Enum('Image, Video, Audio', 'Image')",
		'Sort' => 'Int',
		'AudioPlayer' => 'Text',
    ];

	private static $has_one = [
		'Page' => 'Page',
		'Image' => Image::class,
		'Video' => EmbeddedObject::class

    ];

    private static $owns = [
        'Image'
    ];

	private static $summary_fields = [
		'CMSThumbnail' => 'Item',
		'Title' => 'Title',
		'Type' => 'Type',
    ];

	private static $default_sort = 'Sort';

    private static $allowed_embed_types = array(
        'video'
    );

	public function getCMSFields()
    {
		$fields = parent::getCMSFields();
		$fields->removeByName('VideoID');
		$fields->removeByName('AudioID');
		$fields->removeByName('Sort');
		$fields->removeByName('PageID');

        $fields->addFieldToTab(
			'Root.Main',
            Wrapper::create(
			    FormsEmbeddedObjectField::create('Video', 'Video from oEmbed URL', $this->Video())
				->addExtraClass('stacked')
            )->displayIf('Type')->isEqualTo('Video')->end()
		);


		$fields->dataFieldByName('Image')
			->setAllowedFileCategories('image')
			->setAllowedMaxFileNumber(1)
			->setFolderName('gallery/' . Controller::curr()->currentPageID())
			->displayIf('Type')->isNotEqualTo('Video')->end()
            ->getUpload()->setReplaceFile(true);


		$fields->addFieldToTab(
			'Root.Main',
			TextareaField::create('AudioPlayer', 'Audio Embed Code')
				->displayIf ('Type')->isEqualTo('Audio')->end()
				->setRightTitle('only bandcamp supported, standard player, no artwork')
		);

		return $fields;
	}

	public function getTitle()
    {
		if ($this->Type == 'Video' && $this->Video()->exists()) {
			return $this->Video()->Title;
		} else if ($this->Type == 'Image' && $this->Image()->exists()) {
			return $this->Image()->Title;
		}
	}

	public function getCMSThumbnail()
    {
		if ($this->Type == 'Video' && $this->Video()->exists()) {
			$url = $this->Video()->ThumbURL;
			return DBField::create_field('HTMLVarchar', '<img src="'.$url.'"/>');
		} else if ($this->Image()->exists()) {
			return $this->Image()->CMSThumbnail();
		}
	}

	public function onAfterWrite()
    {
		parent::onAfterWrite();
		if ($this->VideoID && !$this->ImageID && $this->Type == Image::class) {
			$this->Type = 'Video';
			$this->write();
		}

        if ($this->ImageID) {
            if (!$this->Image()->isPublished()) {
                $this->Image()->publishRecursive();
            }
        }
	}

	public function onAfterDelete()
    {
		parent::onAfterDelete();
		if ($this->Image()->exists()) {
			$this->Image()->delete();
		}
		if ($this->Video()->exists()) {
			$this->Video()->delete();
		}
	}
}